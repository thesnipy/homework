grid = int(input('Enter size: ')) #Get size of pyramid
for rows in range(1, grid+1):     # Loop to make above size grid
    for col in range(1, grid-rows+1): # Loop to fill first pattern on all rows ...
        print(" ", end="")            # ...with spaces and remove default \n of print() 
    for col in range(rows,0,-1):      # Loop to print second pattern on all rows in reverse... 
        print(col, end="")            # ...with numbers and remove default \n of print()   
    for col in range(2, rows+1):      # Loop to fill third pattern on the right side, hence(2,..
        print(col, end="")            #..with numbers and remove..     
    print()                           # So that control returns to \n after every line is filled 

grid = int(input('Enter size: '))
for rows in range(1, grid+1):
    for col in range(1, grid-rows+1):
        print(" ", end="")
    for col in range(rows,0,-1):
        print(chr(64+col), end="")   # To print alpha instread on num 
    for col in range(2, rows+1):
        print(chr(64+col), end="")
    print()


